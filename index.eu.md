---
title: Eduki
abstract: |
    Weberako apunteak! 
    Sortu apunteak eta laburpenak (webean zein PDFan)
    Sortu aurkezpenak. 
    Sortu bide ezberdinak maila zein graduaren arabera, eta sortu itzazu hainbat hizkuntzetan
    Guzti hau herraminta bakarrarekin, Eduki!
lang: eu
---

::: note
Nola sortu Edukirentzako apunteak? Jarraitu [Edukiren gida](https://eduki.gitlab.io/eduki.jl) informazio gehiagorako.
:::

# Matematika

* [Ekuazio Diferentzial Arruntak](https://eduki.gitlab.io/odes)
* [Kalkulo](https://eduki.gitlab.io/calculo)