---
title: Eduki
abstract: |
    ¡Apuntes creados para la web! Crea apuntes y resumenes (tanto en la web como en PDF).
    Crea presentaciones en la web. 
    Crea diferentes itinerarios para diferentes cursos/grados, y crea también apuntes multilingües. 
    Todo ello con una sola herramienta, ¡Eduki!
lang: es
createpdf: false
createslides: false
---

::: note
¿Cómo crear apuntes para Eduki? 
Sigue la [Guia de Eduki](https://eduki.gitlab.io/eduki.jl) para más información.
:::

# Matemáticas

* [Cálculo](https://eduki.gitlab.io/calculo)
* [Ecuaciones Diferenciales Ordinarias](https://eduki.gitlab.io/odes)